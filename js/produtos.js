new Vue({

    el: "#produtos",

    beforeMount() {
        let rotaRedirecionada = location.hash.split('=');
        if (rotaRedirecionada[0] === "#/produto") {
            console.log('true :', location.hash);
            this.tituloAcao = 'Produto';
            this.exibeBtnsCadastro = false;
            this.exibeBtnsEdicao = false;
            this.exibeMenuSuperior = false;
            this.consultaProduto(rotaRedirecionada[1]);
        } else {
            console.log('false');
            this.exibirCadastroProduto();
        }

    },


    data: {
        menuCadastroProduto: true,
        menuListarProdutos: false,
        tituloAcao: '',
        arrayProdutos: [],
        exibeBtnsCadastro: true,
        exibeBtnsEdicao: false,
        JsonSalvaEdicao: {},
        arrayProdutosSelecionadosExclusao: [],
        qtdaPaginacaoListaProdutos: 1,
        habilitaBotaoExcluirMultiplosProdutos: false,
        checkBoxMarcado: false,
        checkBoxDesabilitado: false,
        checkBoxDesabilitadoMenu: false,
        paginationVisivel: false,
        paginationAvancaDesabilitado: false,
        paginationVoltaDesabilitado: false,
        qtdaProdutosPorPagina: 20,
        paginaInicial: 1,
        showPages: false,
        page: 1,
        id: '',
        nome: '',
        descricao: '',
        marca: '',
        preco: '',
        qtda: '',
        codigo_barras: '',
        showPages: false,
        shownextBtn: false,
        showPrevBtn: false,
        filter: {},
        money: {
            decimal: ',',
            thousands: '.',
            prefix: 'R$ ',
            suffix: '',
            precision: 2,
            masked: false
        },
        exibeMenuSuperior: true
    },

    methods: {
        exibirCadastroProduto: function () {
            this.tituloAcao = 'Cadastro de Produto';
            this.menuListarProdutos = false;
            this.menuCadastroProduto = true;
            this.exibeBtnsEdicao = false;
            this.exibeBtnsCadastro = true;
            this.paginationVisivel = false;
            this.arrayProdutosSelecionadosExclusao = [];
        },

        exibirListarProduto: function () {
            this.tituloAcao = 'Lista de Produtos';
            this.menuCadastroProduto = false;
            this.menuListarProdutos = true;
            this.paginationVisivel = true;
            this.arrayProdutosSelecionadosExclusao = [];
        },

        exibirEditaProduto: function () {
            this.tituloAcao = 'Editar Produto';
            this.menuCadastroProduto = true;
            this.menuListarProdutos = false;
            this.exibeBtnsCadastro = false;
            this.exibeBtnsEdicao = true;
            this.paginationVisivel = false;
            this.arrayProdutosSelecionadosExclusao = [];
        },

        limparCamposFormularioCadastroProduto: function () {
            this.id = '';
            this.nome = '';
            this.descricao = '';
            this.marca = '';
            this.preco = '';
            this.qtda = '';
            this.codigo_barras = '';
        },

        consultaProduto: async function (id) {
            try {
                const retorno = await axios.get('api/consultar_produto/' + id);
                console.log(retorno.data);

                this.id = retorno.data.IdProduto;
                this.nome = retorno.data.NomeProduto;
                this.descricao = retorno.data.DescricaoProduto;
                this.marca = retorno.data.MarcaProduto;
                this.preco = retorno.data.PrecoProduto;
                this.qtda = retorno.data.QtdaProduto;
                this.codigo_barras = retorno.data.CodigoBarrasProduto;
            } catch (error) {
                console.log(error);
            }
        },

        listarProdutos: async function (page, filter) {
            try {

                this.page = page || this.page;

                this.filter = filter || this.filter;

                this.exibirListarProduto();
                const retorno = await axios.post('api/listar_produtos/' + this.qtdaProdutosPorPagina + '/' + this.page, this.filter);

                let produto = retorno.data;

                this.arrayProdutos = produto.data;
                this.qtdaPaginacaoListaProdutos = produto.pages;

                this.showPages = (produto.pages <= 1);

                this.showPrevBtn = (this.page < 2);
                this.shownextBtn = (this.page >= produto.pages);


            } catch (error) {
                console.log(error);
            }
        },

        SalvaDadosParaEditarProduto: function (indice) {
            try {
                this.exibirEditaProduto();

                let produtos = this.arrayProdutos;
                let produtoEdicao = '';

                for (let i = 0; i < produtos.length; i++) {
                    if (produtos[i].IdProduto === indice) {
                        produtoEdicao = produtos[i];
                    }
                }

                this.id = produtoEdicao.IdProduto;
                this.nome = produtoEdicao.NomeProduto;
                this.descricao = produtoEdicao.DescricaoProduto;
                this.marca = produtoEdicao.MarcaProduto;
                this.preco = produtoEdicao.PrecoProduto;
                this.qtda = produtoEdicao.QtdaProduto;
                this.codigo_barras = produtoEdicao.CodigoBarrasProduto;

            } catch (error) {
                console.log(error);
            }
        },

        salvaEdicaoProduto: async function () {
            try {

                JsonSalvaEdicao = {
                    "id": this.id,
                    "nome": this.nome,
                    "descricao": this.descricao,
                    "marca": this.marca,
                    "preco": this.preco,
                    "qtda": this.qtda,
                    "codigo_barras": this.codigo_barras
                }

                await axios.put('api/editar_produto', JsonSalvaEdicao);

                this.JsonSalvaEdicao = {};
                this.limparCamposFormularioCadastroProduto();
                this.listarProdutos();
                this.exibirListarProduto();

            } catch (error) {
                console.log(error);
            }
        },

        cadastraProduto: async function () {
            try {

                let camposCadastroProduto = {
                    nome: this.nome,
                    descricao: this.descricao,
                    marca: this.marca,
                    preco: this.preco,
                    qtda: this.qtda,
                    codigo_barras: this.codigo_barras
                }

                if (this.verificaCamposEmBranco(camposCadastroProduto) !== 'campos invalidos') {
                    JsonSalvaEdicao = {
                        "nome": this.nome,
                        "descricao": this.descricao,
                        "marca": this.marca,
                        "preco": this.preco,
                        "qtda": this.qtda,
                        "codigo_barras": this.codigo_barras
                    }

                    await axios.post('api/cadastrar_produto', JsonSalvaEdicao);

                    this.JsonSalvaEdicao = {};
                    this.limparCamposFormularioCadastroProduto();

                } else {
                    $('#Modal').modal('toggle');
                }
            } catch (error) {
                console.log(error);
            }
        },

        deletaProduto: async function (indice) {
            try {
                console.log(this.arrayProdutos);
                let produtoExclusao = '';

                for (let i = 0; i < this.arrayProdutos.length; i++) {
                    if (this.arrayProdutos[i].IdProduto === indice) {
                        produtoExclusao = this.arrayProdutos[i];
                        console.log(produtoExclusao);
                    }
                }

                await axios.delete('api/excluir_produto/' + produtoExclusao.IdProduto);

                this.arrayProdutosSelecionadosExclusao = [];
                this.listarProdutos();

            } catch (error) {
                console.log(error);
            }
        },

        selecionaProdutoParaExclusao: async function (indice) {
            try {

                let addIdProdutoNoArrayExclsuao = true;

                if (this.arrayProdutosSelecionadosExclusao.length === 0) {
                    this.arrayProdutosSelecionadosExclusao.push(indice);
                    console.log('primeiro item do array', this.arrayProdutosSelecionadosExclusao[0], '- valor : ', indice)
                } else {
                    for (let indexArray = 0; indexArray < this.arrayProdutosSelecionadosExclusao.length; indexArray++) {
                        console.log('item atual do array', indexArray, ' - id produto: ', this.arrayProdutosSelecionadosExclusao[indexArray])

                        if (this.arrayProdutosSelecionadosExclusao[indexArray] === indice) {
                            addIdProdutoNoArrayExclsuao = false;
                            console.log('existe no array e será removido');
                            this.arrayProdutosSelecionadosExclusao.splice(indexArray, 1);
                        }
                    }

                    if (addIdProdutoNoArrayExclsuao === true) {
                        this.arrayProdutosSelecionadosExclusao.push(indice);
                        console.log('item add no array');
                    }

                }
                console.log('tamanho array: ', this.arrayProdutosSelecionadosExclusao.length)

            } catch (error) {
                console.log(error);
            }
        },

        deletaProdutosSelecionados: async function () {
            try {
                console.log(this.arrayProdutosSelecionadosExclusao);

                await axios.post('api/excluir_varios_produtos', this.arrayProdutosSelecionadosExclusao);

                this.arrayProdutosSelecionadosExclusao = [];
                this.listarProdutos();

                this.checkBoxMarcado = await true;
                this.checkBoxMarcado = await false;

            } catch (error) {
                console.log(error);
            }
        },

        habilitaEDesabilitaCheckBoxExcluiTodosProdutos: function () {
            try {

                if (this.checkBoxMarcado === true) {
                    this.checkBoxMarcado = false;
                    this.checkBoxDesabilitado = false;
                    this.habilitaBotaoExcluirMultiplosProdutos = false;
                } else {
                    this.checkBoxMarcado = true;
                    this.checkBoxDesabilitado = true;
                    this.habilitaBotaoExcluirMultiplosProdutos = true;
                }

            } catch (error) {
                console.log(error);
            }
        },

        deletaTodosProdutosDaLista: async function () {
            try {
                let arrayIdProdutosExclusao = [];
                for (let index = 0; index < this.arrayProdutos.length; index++) {
                    arrayIdProdutosExclusao.push(this.arrayProdutos[index].IdProduto);
                }
                await axios.post('api/excluir_varios_produtos', arrayIdProdutosExclusao);

                this.habilitaBotaoExcluirMultiplosProdutos = false;
                this.checkBoxDesabilitadoMenu = await true;
                this.checkBoxDesabilitadoMenu = await false;
                this.arrayProdutos = [];
                this.arrayProdutosSelecionadosExclusao = [];
                this.checkBoxMarcado = false;
                this.checkBoxDesabilitado = false;
                this.listarProdutos();

            } catch (error) {
                console.log(error);
            }
        },

        verificaCamposEmBranco: function (campos) {
            try {
                for (const key in campos) {
                    if (campos.hasOwnProperty(key)) {
                        if (campos[key] === undefined ||
                            campos[key] === 'undefined' ||
                            campos[key] === '' ||
                            campos[key] === null ||
                            campos[key] === '[]' ||
                            campos[key] === 0
                        ) {
                            return 'campos invalidos';
                        }
                    }
                }
            } catch (error) {
                console.log(error);
            }
        }
    }
})
