new Vue({

    el: "#pedidos",

    beforeMount() {
        this.exibirCadastroPedido();
        this.carregaComboxes();
    },

    computed: {
        valorTotalPedido() {
            this.jsonPedidoSalvar.PrecoTotalItensPedido = this.jsonPedidoSalvar.PrecoUnitarioItensPedido * this.jsonPedidoSalvar.QtdaProdutoItensPedido;
            return this.jsonPedidoSalvar.PrecoTotalItensPedido;
        }
    },

    data: {
        menuCadastroPedido: true,
        menuListarPedidos: false,
        tituloAcao: '',
        arrayPedidos: [],
        exibeBtnsCadastro: true,
        exibeBtnsEdicao: false,
        JsonSalvaEdicao: {},
        arrayPedidosSelecionadosExclusao: [],
        arrayComboProdutos: [],
        arrayComboClientes: [],
        jsonPedidoSalvar: {
            IdCliente: '',
            IdProduto: '',
            IdUsuario: sessionStorage.getItem("id_usuario"),
            DataPedidoCompra: '',
            HorarioPedidoCompra: '',
            StatusPedidoCompra: '',
            PrecoTotalItensPedido: {
                decimal: ',',
                thousands: '.',
                prefix: 'R$ ',
                suffix: '',
                precision: 2,
                masked: false
            },
            PrecoUnitarioItensPedido: {
                decimal: ',',
                thousands: '.',
                prefix: 'R$ ',
                suffix: '',
                precision: 2,
                masked: false
            },
            QtdaProdutoItensPedido: ''
        },
        qtdaPaginacaoListaPedidos: 1,
        habilitaBotaoExcluirMultiplosPedidos: false,
        checkBoxMarcado: false,
        checkBoxDesabilitado: false,
        checkBoxDesabilitadoMenu: false,
        paginationVisivel: false,
        paginationAvancaDesabilitado: false,
        paginationVoltaDesabilitado: false,
        qtdaPedidosPorPagina: 20,
        paginaInicial: 1,
        showPages: false,
        page: 1,
        showPages: false,
        shownextBtn: false,
        showPrevBtn: false,
        filter: {},
        resetComboboxStatus: undefined,
        resetComboboxClientes: undefined,
        resetComboboxProdutos: undefined,
        recebeNomeDoProdutoDoCombobxProduto: '',
        recebeNomeDoClienteDoCombobxProduto: '',
        recebeNomeDoStatusDoCombobxProduto: ''
    },

    methods: {

        verDetalhesDoCliente: function (id_cliente) {
            window.location = "clientes.html" + '#/cliente=' + id_cliente;
        },

        verDetalhesDoProduto: function (id_produto) {
            window.location = "produtos.html" + '#/produto=' + id_produto;
        },

        armazenaIdProduto: function (event) {
            try {
                if (event.target.value !== '') {
                    console.log('cheio');
                    this.jsonPedidoSalvar.IdProduto = event.target.value;
                    console.log(this.jsonPedidoSalvar.IdProduto);
                    this.atribuiNomeDoprodutoNoCamboDeEdicaoDePedido(this.jsonPedidoSalvar.IdProduto);
                } else {
                    console.log('vazio');
                }
            } catch (error) {
                console.log(error);
            }
        },

        atribuiNomeDoprodutoNoCamboDeEdicaoDePedido: async function (id) {
            try {
                const retorno = await axios.get('api/consultar_produto/' + id);
                console.log(retorno.data);
                this.jsonPedidoSalvar.NomeProduto = retorno.data.NomeProduto;
                this.recebeNomeDoProdutoDoCombobxProduto = retorno.data.NomeProduto;
            } catch (error) {
                console.log(error);
            }
        },

        atribuiNomeDoclienteNoCamboDeEdicaoDePedido: async function (id) {
            try {
                const retorno = await axios.get('api/consultar_cliente/' + id);
                console.log(retorno.data);
                this.jsonPedidoSalvar.NomeCliente = retorno.data.NomeCliente;
                this.recebeNomeDoClienteDoCombobxProduto = retorno.data.NomeCliente;
            } catch (error) {
                console.log(error);
            }
        },

        armazenaIdCliente: function (event) {
            if (event.target.value !== '') {
                console.log('cheio');
                this.jsonPedidoSalvar.IdCliente = event.target.value;
                console.log(this.jsonPedidoSalvar.IdCliente);
                this.atribuiNomeDoclienteNoCamboDeEdicaoDePedido(this.jsonPedidoSalvar.IdCliente);
            } else {
                console.log('vazio');
            }
        },

        armazenaStatusPedido: function (event) {
            if (event.target.value !== '') {
                console.log('cheio');
                this.jsonPedidoSalvar.StatusPedidoCompra = event.target.value;
                console.log(this.jsonPedidoSalvar.StatusPedidoCompra);
                this.recebeNomeDoStatusDoCombobxProduto = this.jsonPedidoSalvar.StatusPedidoCompra;
            } else {
                console.log('vazio');
            }
        },

        exibirCadastroPedido: function () {
            this.tituloAcao = 'Cadastro de Pedido';
            this.menuListarPedidos = false;
            this.menuCadastroPedido = true;
            this.exibeBtnsEdicao = false;
            this.exibeBtnsCadastro = true;
            this.paginationVisivel = false;
            this.arrayPedidosSelecionadosExclusao = [];
        },

        exibirListarPedido: function () {
            this.tituloAcao = 'Lista de Pedidos';
            this.menuCadastroPedido = false;
            this.menuListarPedidos = true;
            this.paginationVisivel = true;
            this.arrayPedidosSelecionadosExclusao = [];
        },

        exibirEditaPedido: function () {
            this.tituloAcao = 'Editar Pedido';
            this.menuCadastroPedido = true;
            this.menuListarPedidos = false;
            this.exibeBtnsCadastro = false;
            this.exibeBtnsEdicao = true;
            this.paginationVisivel = false;
            this.arrayPedidosSelecionadosExclusao = [];
        },

        limparCamposFormularioCadastroPedido: function () {

            this.resetComboboxClientes = undefined;
            this.resetComboboxProdutos = undefined;
            this.resetComboboxStatus = undefined;
            this.jsonPedidoSalvar = {
                IdCliente: '',
                IdProduto: '',
                IdUsuario: sessionStorage.getItem("id_usuario"),
                DataPedidoCompra: '',
                HorarioPedidoCompra: '',
                StatusPedidoCompra: '',
                PrecoTotalItensPedido: {
                    decimal: ',',
                    thousands: '.',
                    prefix: 'R$ ',
                    suffix: '',
                    precision: 2,
                    masked: false
                },
                PrecoUnitarioItensPedido: {
                    decimal: ',',
                    thousands: '.',
                    prefix: 'R$ ',
                    suffix: '',
                    precision: 2,
                    masked: false
                },
                QtdaProdutoItensPedido: ''
            }
        },

        carregaComboxes: async function (page, filter) {
            try {

                const retorno1 = await axios.get('api/listar_todos_produtos');
                this.arrayComboProdutos = retorno1.data;
                console.log(this.arrayComboProdutos);

                const retorno2 = await axios.get('api/listar_todos_clientes');
                this.arrayComboClientes = retorno2.data;
                console.log(this.arrayComboClientes);

            } catch (error) {
                console.log(error);
            }
        },

        listarPedidos: async function (page, filter) {
            try {

                this.page = page || this.page;

                this.filter = filter || this.filter;

                this.exibirListarPedido();
                const retorno = await axios.post('api/listar_pedidos/' + this.qtdaPedidosPorPagina + '/' + this.page, this.filter);

                console.log(retorno.data.data[0]);

                let pedido = retorno.data;

                this.arrayPedidos = pedido.data;
                this.qtdaPaginacaoListaPedidos = pedido.pages;

                this.showPages = (pedido.pages <= 1);

                this.showPrevBtn = (this.page < 2);
                this.shownextBtn = (this.page >= pedido.pages);


            } catch (error) {
                console.log(error);
            }
        },

        SalvaDadosParaEditarPedido: function (indice) {
            try {
                this.exibirEditaPedido();

                let pedidos = this.arrayPedidos;
                let pedidoEdicao = '';

                for (let i = 0; i < pedidos.length; i++) {
                    if (pedidos[i].IdPedidoCompra === indice) {
                        pedidoEdicao = pedidos[i];
                    }
                }

                this.jsonPedidoSalvar.IdPedidoCompra = pedidoEdicao.IdPedidoCompra;
                this.jsonPedidoSalvar.IdUsuario = pedidoEdicao.IdUsuario;
                this.atribuiNomeDoclienteNoCamboDeEdicaoDePedido(pedidoEdicao.IdCliente);
                this.jsonPedidoSalvar.IdCliente = pedidoEdicao.IdCliente;
                this.atribuiNomeDoprodutoNoCamboDeEdicaoDePedido(pedidoEdicao.IdProduto);
                this.jsonPedidoSalvar.IdProduto = pedidoEdicao.IdProduto;
                this.jsonPedidoSalvar.DataPedidoCompra = pedidoEdicao.DataPedidoCompra;
                this.jsonPedidoSalvar.HorarioPedidoCompra = pedidoEdicao.HorarioPedidoCompra;
                this.jsonPedidoSalvar.StatusPedidoCompra = pedidoEdicao.StatusPedidoCompra;
                this.jsonPedidoSalvar.PrecoUnitarioItensPedido = pedidoEdicao.PrecoUnitarioItensPedido;
                this.jsonPedidoSalvar.QtdaProdutoItensPedido = pedidoEdicao.QtdaProdutoItensPedido;
                this.jsonPedidoSalvar.PrecoTotalItensPedido = pedidoEdicao.PrecoTotalItensPedido;

                console.log(this.jsonPedidoSalvar.IdPedidoCompra);

            } catch (error) {
                console.log(error);
            }
        },

        salvaEdicaoPedido: async function () {
            try {

                JsonSalvaEdicao = {
                    "id_pedido": this.jsonPedidoSalvar.IdPedidoCompra,
                    "id_usuario": this.jsonPedidoSalvar.IdUsuario,
                    "id_cliente": this.jsonPedidoSalvar.IdCliente,
                    "id_produto": this.jsonPedidoSalvar.IdProduto,
                    "data_pedido": this.jsonPedidoSalvar.DataPedidoCompra,
                    "horario_pedido": this.jsonPedidoSalvar.HorarioPedidoCompra,
                    "status_pedido": this.jsonPedidoSalvar.StatusPedidoCompra,
                    "preco_unitario": this.jsonPedidoSalvar.PrecoUnitarioItensPedido,
                    "qtda_produto": this.jsonPedidoSalvar.QtdaProdutoItensPedido,
                    "preco_total": this.jsonPedidoSalvar.PrecoTotalItensPedido
                }

                console.log('para salvar: ', JsonSalvaEdicao);
                await axios.put('api/editar_pedido', JsonSalvaEdicao);

                this.JsonSalvaEdicao = {};
                this.limparCamposFormularioCadastroPedido();
                this.listarPedidos();
                this.exibirListarPedido();

            } catch (error) {
                console.log(error);
            }
        },

        cadastraPedido: async function () {
            try {

                let camposCadastroProduto = {
                    id_usuario: this.jsonPedidoSalvar.IdUsuario,
                    id_cliente: this.jsonPedidoSalvar.IdCliente,
                    id_produto: this.jsonPedidoSalvar.IdProduto,
                    data_pedido_compra: this.jsonPedidoSalvar.DataPedidoCompra,
                    horario_pedido_compra: this.jsonPedidoSalvar.HorarioPedidoCompra,
                    status_pedido: this.jsonPedidoSalvar.StatusPedidoCompra,
                    preco_unitario: this.jsonPedidoSalvar.PrecoUnitarioItensPedido,
                    qtda_produto: this.jsonPedidoSalvar.QtdaProdutoItensPedido,
                    preco_total: this.jsonPedidoSalvar.PrecoTotalItensPedido
                }

                console.log(camposCadastroProduto);

                if (this.verificaCamposEmBranco(camposCadastroProduto) !== 'campos invalidos') {
                    JsonSalvaEdicao = {
                        "id_usuario": this.jsonPedidoSalvar.IdUsuario,
                        "id_cliente": this.jsonPedidoSalvar.IdCliente,
                        "id_produto": this.jsonPedidoSalvar.IdProduto,
                        "data_pedido_compra": this.jsonPedidoSalvar.DataPedidoCompra,
                        "horario_pedido_compra": this.jsonPedidoSalvar.HorarioPedidoCompra,
                        "status_pedido": this.jsonPedidoSalvar.StatusPedidoCompra,
                        "preco_unitario": this.jsonPedidoSalvar.PrecoUnitarioItensPedido,
                        "qtda_produto": this.jsonPedidoSalvar.QtdaProdutoItensPedido,
                        "preco_total": this.jsonPedidoSalvar.PrecoTotalItensPedido
                    }

                    console.log(JsonSalvaEdicao);

                    await axios.post('api/cadastrar_pedido', JsonSalvaEdicao);

                    this.JsonSalvaEdicao = {};
                    this.limparCamposFormularioCadastroPedido();

                } else {
                    $('#Modal').modal('toggle');
                }
            } catch (error) {
                console.log(error);
            }
        },

        deletaPedido: async function (indice) {
            try {
                console.log('indice: ', indice);
                console.log(this.arrayPedidos);
                let pedidoExclusao = '';

                for (let i = 0; i < this.arrayPedidos.length; i++) {
                    if (this.arrayPedidos[i].IdPedidoCompra === indice) {
                        pedidoExclusao = this.arrayPedidos[i];
                        console.log(pedidoExclusao);
                    }
                }

                console.log('pedido para deletar: ', pedidoExclusao.IdPedidoCompra);
                await axios.delete('api/excluir_pedido/' + pedidoExclusao.IdPedidoCompra);

                this.arrayPedidosSelecionadosExclusao = [];
                this.listarPedidos();

            } catch (error) {
                console.log(error);
            }
        },

        selecionaPedidoParaExclusao: async function (indice) {
            try {

                let addIdProdutoNoArrayExclsuao = true;

                if (this.arrayPedidosSelecionadosExclusao.length === 0) {
                    this.arrayPedidosSelecionadosExclusao.push(indice);
                    console.log('primeiro item do array', this.arrayPedidosSelecionadosExclusao[0], '- valor : ', indice)
                } else {
                    for (let indexArray = 0; indexArray < this.arrayPedidosSelecionadosExclusao.length; indexArray++) {
                        console.log('item atual do array', indexArray, ' - id produto: ', this.arrayPedidosSelecionadosExclusao[indexArray])

                        if (this.arrayPedidosSelecionadosExclusao[indexArray] === indice) {
                            addIdProdutoNoArrayExclsuao = false;
                            console.log('existe no array e será removido');
                            this.arrayPedidosSelecionadosExclusao.splice(indexArray, 1);
                        }
                    }

                    if (addIdProdutoNoArrayExclsuao === true) {
                        this.arrayPedidosSelecionadosExclusao.push(indice);
                        console.log('item add no array');
                    }

                }
                console.log('tamanho array: ', this.arrayPedidosSelecionadosExclusao.length)

            } catch (error) {
                console.log(error);
            }
        },

        deletaPedidosSelecionados: async function () {
            try {
                console.log(this.arrayPedidosSelecionadosExclusao);

                await axios.post('api/excluir_varios_pedidos', this.arrayPedidosSelecionadosExclusao);

                this.arrayPedidosSelecionadosExclusao = [];
                this.listarPedidos();

                this.checkBoxMarcado = await true;
                this.checkBoxMarcado = await false;

            } catch (error) {
                console.log(error);
            }
        },

        habilitaEDesabilitaCheckBoxExcluiTodosPedidos: function () {
            try {

                if (this.checkBoxMarcado === true) {
                    this.checkBoxMarcado = false;
                    this.checkBoxDesabilitado = false;
                    this.habilitaBotaoExcluirMultiplosPedidos = false;
                } else {
                    this.checkBoxMarcado = true;
                    this.checkBoxDesabilitado = true;
                    this.habilitaBotaoExcluirMultiplosPedidos = true;
                }

            } catch (error) {
                console.log(error);
            }
        },

        deletaTodosPedidosDaLista: async function () {
            try {
                let arrayIdProdutosExclusao = [];
                for (let index = 0; index < this.arrayPedidos.length; index++) {
                    arrayIdProdutosExclusao.push(this.arrayPedidos[index].IdProduto);
                }
                await axios.post('api/excluir_varios_produtos', arrayIdProdutosExclusao);

                this.habilitaBotaoExcluirMultiplosPedidos = false;
                this.checkBoxDesabilitadoMenu = await true;
                this.checkBoxDesabilitadoMenu = await false;
                this.arrayPedidos = [];
                this.arrayPedidosSelecionadosExclusao = [];
                this.checkBoxMarcado = false;
                this.checkBoxDesabilitado = false;
                this.listarPedidos();

            } catch (error) {
                console.log(error);
            }
        },

        verificaCamposEmBranco: function (campos) {
            try {
                for (const key in campos) {
                    if (campos.hasOwnProperty(key)) {
                        if (campos[key] === undefined ||
                            campos[key] === 'undefined' ||
                            campos[key] === '' ||
                            campos[key] === null ||
                            campos[key] === '[]' ||
                            campos[key] === 0
                        ) {
                            return 'campos invalidos';
                        }
                    }
                }
            } catch (error) {
                console.log(error);
            }
        }
    }
})
