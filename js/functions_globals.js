// Função responsável por armazenar o token enviado pela API no SessionLocalStorage do navegador ...
function armazenaIdUsuario(id) {
    try {
        /* Verifica se o navegador tem suporte ao armazenamento local
        e salva o token */
        if (typeof (Storage) !== "undefined") {
            // Armazena o token ...
            sessionStorage.setItem("id_usuario", id);
            return true;
        } else {
            console.log('Navegador não tem suporte ao sessionStorage');
            return false;
        }
    } catch (error) {
        console.log(error);
    }
}

// Função retorna o token armazenado no SessionLocalStorage do navegador ...
function getIdUsuario() {
    return sessionStorage.getItem("id_usuario");
    
}
