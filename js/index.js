// Instância do VueJS
new Vue({

    // instânciando elemento root para vincular ao VueJS.
    el: "#index",

    // Variáveis do formulário/modal de login
    data: {
        usuario: '',
        senha: '',
        msgUsuarioNaoValidado: 'Usuário inexistente',
        msgCamposEmBranco: 'Por favor, informe usuário e senha',
        modalUsuarioNaoValidado: false,
        modalCamposEmBranco: false

    },

    // Hook - Função nativa do VueJS - Assim que o template Html tiver carregado, faça ...
    mounted() {
        // Chame a função para setar foco no primeiro campo do formulário (campo usuario)
        this.focus();
    },
    // Funções ...
    methods: {

        // Função para setar foco no campo usuario
        focus: function () {
            this.$refs.usuario.focus();
        },

        /*  Função para validar se campos estão em branco e para realizar login 
         *  caso preencidos coretamente - campos preenchidos (true), caso contrário (false)
         */
        validaCampos: function () {
            try {
                if (this.usuario === '' || this.senha === '') {
                    return false;
                } else {
                    return true
                }
            } catch (error) {
                console.log(error);
            }
        },

        loginUsuario: async function () {
            try {
                // Verifica se há campos em branco, caso retorne false (há campo(s) em branco), caso contrário true ...
                if (this.validaCampos() === true) {

                    // Json que recebe o nome de usuário e senha
                    let dadosEnvio = {
                        usuario: this.usuario,
                        senha: this.senha
                    }

                    // Passando o Json como parâmetro para a API 
                    let retorno = await axios.post('api/login_usuario', dadosEnvio);

                    // Se o usuário existir no bd
                    if (retorno.data !== false) {
                        
                        sessionStorage.clear();
                        armazenaIdUsuario(retorno.data.IdUsuario);

                        this.limpaCamposLogin();
                        location.href = "menu.html";
                    } else {
                        // Chama modal informando que o usuario não existe no bd ...
                        this.exibeModalUsuarioInexistente();
                        this.limpaCamposLogin();
                    }
                } else {
                    // Chama modal informando que há campos em branco 
                    this.exibeModalCamposEmBranco();
                    this.limpaCamposLogin();
                }
            } catch (error) {
                console.log(error);
            }
        },

        // Exibe modal de campos de login em branco
        exibeModalCamposEmBranco: function () {
            this.modalCamposEmBranco = true;
            this.modalUsuarioNaoValidado = false;
            $('#Modal').modal('toggle');
        },

        // Exibe modal de usuário inexistente
        exibeModalUsuarioInexistente: function () {
            this.modalCamposEmBranco = false;
            this.modalUsuarioNaoValidado = true;
            $('#Modal').modal('toggle');
        },

        // Limpa campos de login
        limpaCamposLogin: function () {
            this.usuario = '';
            this.senha = '';
        }
    }
})

