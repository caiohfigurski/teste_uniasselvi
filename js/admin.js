// Instância do VueJS
new Vue({

    // instânciando elemento root para vincular ao VueJS.
    el: "#admin",

    // Variáveis do formulário/modal de login - acesso administrativo.
    data: {
        pessoas: [],
        token: '',
        cpf: '',
        modalCamposEmBranco: false,
        modalCpfInvalido: false,
        modalCpfNaoCadastrado :false,
        msgCamposEmBranco: 'Por favor insira um CPF',
        msgCpfInvalido: 'Por favor insira um CPF válido',
        msgCpfNaoCadastrado: 'CPF não cadastrado',
        nome: '',
        cep: '',
        bairro: '',
        rua: '',
        numero: '',
        cidade: '',
        uf: '',
        formularioExibicao: false
    },

    // Hook - Função nativa do VueJS - Assim que o template Html tiver carregado, faça ...
    mounted() {
        // Chame as funções abaixo quando a rota 'admin.html' for exibida ...
        this.validaToken();
        this.listarPessoas();
        this.focus();
    },

    // Funções ...
    methods: {

        // Função para setar foco ...
        focus: function () {
            this.$refs.cpf.focus();
        },

        // Função para validar se o token armazenado local é ainda válido para realizar requisições na API
        validaToken: async function () {
            try {
                // Recebe o token armazenado localmente ...
                const token = getTokenLocal();

                // Se o token existir localmente e não estiver vazio ...
                if (token && token !== null) {
                    // Envia o token para a API validar ...
                    const tokenValidado = await axios.post('api/valida_token', { token: token });

                    // Verifica se o toquem for valido (true) ...
                    if (tokenValidado.data === true) {
                        // Armazena o token em uma variável local ...
                        this.token = token;

                        // Caso não valide redirecione para página de login ...
                    } else {
                        // Redireciona para tela de login
                        location.href = "login.html";
                    }
                    // Caso token não exista ...
                } else {
                    // Redireciona para tela de login
                    location.href = "login.html";
                }
            } catch (error) {
                console.log(error);
            }
        },

        // Função para retornar as informações do CPF consultado ...
        consultaCPF: async function () {
            try {
                if (await this.validaCampos()) {
                    console.log(this.validaCampos());
                    // Recebe objeto Json com as informações do cpf que foi consultado ...
                    let cpfRetornado = await axios.get('api/consulta_cpf/' + [this.cpf + '/' + this.token]);
                    console.log(cpfRetornado);
                    /* Verifica se o objeto retornado possui o campo 'cpf',
                     * caso tenha singnifica que o token não expirou e que é possivel
                     * ainda fazer requisiçoes à API ...
                     * Observação: caso o token tenha expirado será retornado uma string
                     * tratada diretamente na API ...
                     */
                    if (cpfRetornado.data.cpf) {
                        this.formularioExibicao = true
                        this.nome = cpfRetornado.data.nome;
                        this.cep = cpfRetornado.data.cep;
                        this.bairro = cpfRetornado.data.bairro;
                        this.rua = cpfRetornado.data.rua;
                        this.numero = cpfRetornado.data.numero;
                        this.cidade = cpfRetornado.data.cidade;
                        this.uf = cpfRetornado.data.estado;
                    } else {
                        // Chama modal informando que o CPF é inválido ...
                        this.modalCpfInvalido = false;
                        this.modalCamposEmBranco = false;
                        this.modalCpfNaoCadastrado = true;
                        
                        $('#Modal').modal('toggle');
                        this.formularioExibicao = false;
                    }
                } else {
                    // Chama modal informando que ou o campo está em branco ...
                    $('#Modal').modal('toggle');
                    this.formularioExibicao = false;
                }
            } catch (error) {
                console.log(error);
            }
        },

        // Função para validar se o campo de busca do CPF está em branco e para validar o CPF ...
        validaCampos: async function () {
            if (this.cpf === '') {

                // Modal para campo em branco ...
                this.modalCpfInvalido = false;
                this.modalCpfNaoCadastrado = false;
                this.modalCamposEmBranco = true;
                return false;
            } else {

                /* Caso os campo de consulta CPF esteja ok, valide o CPF ...
                *  Variável recebe objeto JSON com informação contento true (cpf válido) ou false (inválido).
                */
                let cpfValido = await axios.get('api/valida_cpf/' + this.cpf);

                if (cpfValido.data === true) {
                    return true;
                } else {
                    // Limpa o campo do CPF ...
                    this.cpf = '';
                    
                    // modal para cpf inválido ...
                    this.modalCpfInvalido = true;
                    this.modalCamposEmBranco = false;
                    this.modalCpfNaoCadastrado = false;
                    return false;
                }
            }
        },

        // Função para realizar logoff da tela administrativa e para limpar o token do armazenamento local ...
        logOut: function () {
            try {
                // Prompt solicitando confirmação para encerrar fazer logoff ...
                if (confirm('Deseja fazer sair ?')) {
                    // Limpa o token da variável local ...
                    this.token = '';

                    // Limpa o token do armazenamento local do navegador ...
                    sessionStorage.clear();

                    //Redireciona para a pagina administrativa ...
                    location.href = "index.html";
                }
            } catch (error) {
                console.log(error);
            }
        },

        listarPessoas: async function () {
            try {
                // Variável recebe Json com os dados do banco ...
                let retorno = await axios.get('api/listar_pessoas');

                // Array recebe os dados para listar na tabela do front-end
                this.pessoas = retorno.data;
            } catch (error) {
                console.log(error);
            }
        }
    }
})

