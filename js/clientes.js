new Vue({

    el: "#clientes",

    beforeMount() {
        let rotaRedirecionada = location.hash.split('=');
        if (rotaRedirecionada[0] === "#/cliente") {
            console.log('true :', location.hash);
            this.tituloAcao = 'Cliente';
            this.exibeBtnsCadastro = false;
            this.exibeBtnsEdicao = false;
            this.exibeMenuSuperior = false;
            this.consultaCliente(rotaRedirecionada[1]);
        } else {
            console.log('false');
            this.exibirCadastroCliente();
        }

    },


    data: {
        menuCadastroCliente: true,
        menuListarClientes: false,
        tituloAcao: '',
        arrayClientes: [],
        exibeBtnsCadastro: true,
        exibeBtnsEdicao: false,
        JsonSalvaEdicao: {},
        arrayClientesSelecionadosExclusao: [],
        qtdaPaginacaoListaClientes: 1,
        habilitaBotaoExcluirMultiplosUsuarios: false,
        checkBoxMarcado: false,
        checkBoxDesabilitado: false,
        checkBoxDesabilitadoMenu: false,
        paginationVisivel: false,
        paginationAvancaDesabilitado: false,
        paginationVoltaDesabilitado: false,
        qtdaClientesPorPagina: 20,
        paginaInicial: 1,
        showPages: false,
        page: 1,
        id: '',
        nome: '',
        endereco: '',
        cpf: '',
        cep: '',
        bairro: '',
        numero: '',
        email: '',
        nascimento: '',
        pais: '',
        cidade: '',
        estado: '',
        celular: '',
        telefone: '',
        showPages: false,
        shownextBtn: false,
        showPrevBtn: false,
        filter: {},
        exibeMenuSuperior: true
    },

    methods: {
        exibirCadastroCliente: function () {
            this.tituloAcao = 'Cadastro de Cliente';
            this.menuListarClientes = false;
            this.menuCadastroCliente = true;
            this.exibeBtnsEdicao = false;
            this.exibeBtnsCadastro = true;
            this.paginationVisivel = false;
            this.arrayClientesSelecionadosExclusao = [];
        },

        exibirListarCliente: function () {
            this.tituloAcao = 'Lista de Clientes';
            this.menuCadastroCliente = false;
            this.menuListarClientes = true;
            this.paginationVisivel = true;
            this.arrayClientesSelecionadosExclusao = [];
        },

        exibirEditaCliente: function () {
            this.tituloAcao = 'Editar Cliente';
            this.menuCadastroCliente = true;
            this.menuListarClientes = false;
            this.exibeBtnsCadastro = false;
            this.exibeBtnsEdicao = true;
            this.paginationVisivel = false;
            this.arrayClientesSelecionadosExclusao = [];
        },

        limparCamposFormularioCadastroPessoa: function () {
            this.nome = '';
            this.cpf = '';
            this.nascimento = '';
            this.endereco = '';
            this.numero = '';
            this.bairro = '';
            this.cidade = '';
            this.estado = '';
            this.pais = '';
            this.telefone = '';;
            this.celular = '';
            this.email = '';
            this.cep = '';
        },

        consultaCliente: async function (id) {
            try {
                const retorno = await axios.get('api/consultar_cliente/' + id);
                console.log(retorno.data);

                    this.id = retorno.data.IdCliente,
                    this.nome = retorno.data.NomeCliente,
                    this.cpf = retorno.data.CpfCliente,
                    this.nascimento = retorno.data.DataNascimento,
                    this.endereco = retorno.data.EnderecoCliente,
                    this.numero = retorno.data.NumMoradiaCliente,
                    this.bairro = retorno.data.BairroCliente,
                    this.cidade = retorno.data.CidadeCliente,
                    this.estado = retorno.data.EstadoCliente,
                    this.pais = retorno.data.PaisCliente,
                    this.telefone = retorno.data.TelefoneCliente,
                    this.celular = retorno.data.CelularCliente,
                    this.email = retorno.data.EmailCliente,
                    this.cep = retorno.data.CepCliente
            } catch (error) {
                console.log(error);
            }
        },

        listarClientes: async function (page, filter) {
            try {

                this.page = page || this.page;

                this.filter = filter || this.filter;

                this.exibirListarCliente();
                const retorno = await axios.post('api/listar_clientes/' + this.qtdaClientesPorPagina + '/' + this.page, this.filter);

                let cliente = retorno.data;

                this.arrayClientes = cliente.data;
                this.qtdaPaginacaoListaClientes = cliente.pages;

                this.showPages = (cliente.pages <= 1);

                this.showPrevBtn = (this.page < 2);
                this.shownextBtn = (this.page >= cliente.pages);


            } catch (error) {
                console.log(error);
            }
        },

        SalvaDadosParaEditarCliente: function (indice) {
            try {
                this.exibirEditaCliente();

                let clientes = this.arrayClientes;
                let clienteEdicao = '';

                for (let i = 0; i < clientes.length; i++) {
                    if (clientes[i].IdCliente === indice) {
                        clienteEdicao = clientes[i];
                    }
                }

                this.id = clienteEdicao.IdCliente,
                    this.nome = clienteEdicao.NomeCliente,
                    this.cpf = clienteEdicao.CpfCliente,
                    this.nascimento = clienteEdicao.DataNascimento,
                    this.endereco = clienteEdicao.EnderecoCliente,
                    this.numero = clienteEdicao.NumMoradiaCliente,
                    this.bairro = clienteEdicao.BairroCliente,
                    this.cidade = clienteEdicao.CidadeCliente,
                    this.estado = clienteEdicao.EstadoCliente,
                    this.pais = clienteEdicao.PaisCliente,
                    this.telefone = clienteEdicao.TelefoneCliente,
                    this.celular = clienteEdicao.CelularCliente,
                    this.email = clienteEdicao.EmailCliente,
                    this.cep = clienteEdicao.CepCliente

            } catch (error) {
                console.log(error);
            }
        },

        salvaEdicaoCliente: async function () {
            try {

                JsonSalvaEdicao = {
                    "id": this.id,
                    "nome": this.nome,
                    "cpf": this.cpf,
                    "data_nascimento": this.nascimento,
                    "endereco": this.endereco,
                    "numero_moradia": this.numero,
                    "bairro": this.bairro,
                    "cidade": this.cidade,
                    "estado": this.estado,
                    "pais": this.pais,
                    "telefone": this.telefone,
                    "celular": this.celular,
                    "email": this.email,
                    "cep": this.cep
                }

                await axios.put('api/editar_cliente', JsonSalvaEdicao);

                this.JsonSalvaEdicao = {};
                this.limparCamposFormularioCadastroPessoa();
                this.listarClientes();
                this.exibirListarCliente();

            } catch (error) {
                console.log(error);
            }
        },

        cadastraCliente: async function () {
            try {

                let camposCadastroCliente = {
                    nome: this.nome,
                    endereco: this.endereco,
                    cpf: this.cpf,
                    cep: this.cep,
                    bairro: this.bairro,
                    numero: this.numero,
                    email: this.email,
                    nascimento: this.nascimento,
                    pais: this.pais,
                    cidade: this.cidade,
                    estado: this.estado,
                    celular: this.celular,
                    telefone: this.telefone
                }

                if (this.verificaCamposEmBranco(camposCadastroCliente) !== 'campos invalidos') {
                    JsonSalvaEdicao = {
                        "nome": this.nome,
                        "cpf": this.cpf,
                        "data_nascimento": this.nascimento,
                        "endereco": this.endereco,
                        "numero_moradia": this.numero,
                        "bairro": this.bairro,
                        "cidade": this.cidade,
                        "estado": this.estado,
                        "pais": this.pais,
                        "telefone": this.telefone,
                        "celular": this.celular,
                        "email": this.email,
                        "cep": this.cep
                    }

                    await axios.post('api/cadastrar_cliente', JsonSalvaEdicao);

                    this.JsonSalvaEdicao = {};
                    this.limparCamposFormularioCadastroPessoa();

                } else {
                    $('#Modal').modal('toggle');
                }
            } catch (error) {
                console.log(error);
            }
        },

        deletaCliente: async function (indice) {
            try {
                let clienteExclusao = '';

                for (let i = 0; i < this.arrayClientes.length; i++) {
                    if (this.arrayClientes[i].IdCliente === indice) {
                        clienteExclusao = this.arrayClientes[i];
                    }
                }

                console.log(clienteExclusao.IdCliente);
                await axios.delete('api/excluir_cliente/' + clienteExclusao.IdCliente);

                this.arrayClientesSelecionadosExclusao = [];
                this.listarClientes();

            } catch (error) {
                console.log(error);
            }
        },

        selecionaClienteParaExclusao: async function (indice) {
            try {

                let addIdClienteNoArrayExclsuao = true;

                if (this.arrayClientesSelecionadosExclusao.length === 0) {
                    this.arrayClientesSelecionadosExclusao.push(indice);
                    console.log('primeiro item do array', this.arrayClientesSelecionadosExclusao[0], '- valor : ', indice)
                } else {
                    for (let indexArray = 0; indexArray < this.arrayClientesSelecionadosExclusao.length; indexArray++) {
                        console.log('item atual do array', indexArray, ' - id cliente: ', this.arrayClientesSelecionadosExclusao[indexArray])

                        if (this.arrayClientesSelecionadosExclusao[indexArray] === indice) {
                            addIdClienteNoArrayExclsuao = false;
                            console.log('existe no array e será removido');
                            this.arrayClientesSelecionadosExclusao.splice(indexArray, 1);
                        }
                    }

                    if (addIdClienteNoArrayExclsuao === true) {
                        this.arrayClientesSelecionadosExclusao.push(indice);
                        console.log('item add no array');
                    }

                }
                console.log('tamanho array: ', this.arrayClientesSelecionadosExclusao.length)

            } catch (error) {
                console.log(error);
            }
        },

        deletaClientesSelecionados: async function () {
            try {
                console.log(this.arrayClientesSelecionadosExclusao);

                await axios.post('api/excluir_varios_clientes', this.arrayClientesSelecionadosExclusao);

                this.arrayClientesSelecionadosExclusao = [];
                this.listarClientes();

                this.checkBoxMarcado = await true;
                this.checkBoxMarcado = await false;

            } catch (error) {
                console.log(error);
            }
        },

        habilitaEDesabilitaCheckBoxExcluiTodosClientes: function () {
            try {

                if (this.checkBoxMarcado === true) {
                    this.checkBoxMarcado = false;
                    this.checkBoxDesabilitado = false;
                    this.habilitaBotaoExcluirMultiplosUsuarios = false;
                } else {
                    this.checkBoxMarcado = true;
                    this.checkBoxDesabilitado = true;
                    this.habilitaBotaoExcluirMultiplosUsuarios = true;
                }

            } catch (error) {
                console.log(error);
            }
        },

        deletaTodosClientesDaLista: async function () {
            try {
                let arrayIdClientesExclusao = [];
                for (let index = 0; index < this.arrayClientes.length; index++) {
                    arrayIdClientesExclusao.push(this.arrayClientes[index].IdCliente);
                }
                await axios.post('api/excluir_varios_clientes', arrayIdClientesExclusao);

                this.habilitaBotaoExcluirMultiplosUsuarios = false;
                this.checkBoxDesabilitadoMenu = await true;
                this.checkBoxDesabilitadoMenu = await false;
                this.arrayClientes = [];
                this.arrayClientesSelecionadosExclusao = [];
                this.checkBoxMarcado = false;
                this.checkBoxDesabilitado = false;
                this.listarClientes();

            } catch (error) {
                console.log(error);
            }
        },

        verificaCamposEmBranco: function (campos) {
            try {
                for (const key in campos) {
                    if (campos.hasOwnProperty(key)) {
                        if (campos[key] === undefined ||
                            campos[key] === 'undefined' ||
                            campos[key] === '' ||
                            campos[key] === null ||
                            campos[key] === '[]' ||
                            campos[key] === 0
                        ) {
                            return 'campos invalidos';
                        }
                    }
                }
            } catch (error) {
                console.log(error);
            }
        }
    }
})
