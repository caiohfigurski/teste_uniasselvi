-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 30-Set-2019 às 01:34
-- Versão do servidor: 10.1.38-MariaDB
-- versão do PHP: 7.3.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `teste_uniasselvi`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `clientes`
--

CREATE TABLE `clientes` (
  `IdCliente` int(11) NOT NULL,
  `NomeCliente` varchar(100) DEFAULT NULL,
  `CpfCliente` varchar(14) DEFAULT NULL,
  `DataNascimento` varchar(10) DEFAULT NULL,
  `EnderecoCliente` varchar(100) DEFAULT NULL,
  `NumMoradiaCliente` int(11) DEFAULT NULL,
  `BairroCliente` varchar(100) DEFAULT NULL,
  `CidadeCliente` varchar(100) DEFAULT NULL,
  `EstadoCliente` varchar(100) DEFAULT NULL,
  `PaisCliente` varchar(100) DEFAULT NULL,
  `TelefoneCliente` varchar(15) DEFAULT NULL,
  `CelularCliente` varchar(15) DEFAULT NULL,
  `EmailCliente` varchar(100) DEFAULT NULL,
  `CepCliente` varchar(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `pedidos_de_compras`
--

CREATE TABLE `pedidos_de_compras` (
  `IdPedidoCompra` int(11) NOT NULL,
  `IdUsuario` int(11) DEFAULT NULL,
  `IdCliente` int(11) DEFAULT NULL,
  `IdProduto` int(11) DEFAULT NULL,
  `DataPedidoCompra` varchar(10) DEFAULT NULL,
  `HorarioPedidoCompra` varchar(10) DEFAULT NULL,
  `StatusPedidoCompra` varchar(9) DEFAULT NULL,
  `PrecoUnitarioItensPedido` varchar(100) DEFAULT NULL,
  `QtdaProdutoItensPedido` bigint(100) DEFAULT NULL,
  `PrecoTotalItensPedido` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `produtos`
--

CREATE TABLE `produtos` (
  `IdProduto` int(11) NOT NULL,
  `NomeProduto` varchar(100) DEFAULT NULL,
  `DescricaoProduto` varchar(100) DEFAULT NULL,
  `MarcaProduto` varchar(100) DEFAULT NULL,
  `PrecoProduto` varchar(100) DEFAULT NULL,
  `QtdaProduto` int(100) DEFAULT NULL,
  `CodigoBarrasProduto` int(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `usuarios`
--

CREATE TABLE `usuarios` (
  `IdUsuario` int(11) NOT NULL,
  `NomeUsuario` varchar(100) DEFAULT NULL,
  `SenhaUsuario` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `usuarios`
--

INSERT INTO `usuarios` (`IdUsuario`, `NomeUsuario`, `SenhaUsuario`) VALUES
(1, 'uniasselvi', 'uniasselvi');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `clientes`
--
ALTER TABLE `clientes`
  ADD PRIMARY KEY (`IdCliente`);

--
-- Indexes for table `pedidos_de_compras`
--
ALTER TABLE `pedidos_de_compras`
  ADD PRIMARY KEY (`IdPedidoCompra`),
  ADD KEY `IdUsuario` (`IdUsuario`),
  ADD KEY `IdCliente` (`IdCliente`),
  ADD KEY `IdProduto` (`IdProduto`);

--
-- Indexes for table `produtos`
--
ALTER TABLE `produtos`
  ADD PRIMARY KEY (`IdProduto`);

--
-- Indexes for table `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`IdUsuario`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `clientes`
--
ALTER TABLE `clientes`
  MODIFY `IdCliente` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pedidos_de_compras`
--
ALTER TABLE `pedidos_de_compras`
  MODIFY `IdPedidoCompra` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `produtos`
--
ALTER TABLE `produtos`
  MODIFY `IdProduto` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `IdUsuario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Constraints for dumped tables
--

--
-- Limitadores para a tabela `pedidos_de_compras`
--
ALTER TABLE `pedidos_de_compras`
  ADD CONSTRAINT `pedidos_de_compras_ibfk_1` FOREIGN KEY (`IdUsuario`) REFERENCES `usuarios` (`IdUsuario`),
  ADD CONSTRAINT `pedidos_de_compras_ibfk_2` FOREIGN KEY (`IdCliente`) REFERENCES `clientes` (`IdCliente`),
  ADD CONSTRAINT `pedidos_de_compras_ibfk_3` FOREIGN KEY (`IdProduto`) REFERENCES `produtos` (`IdProduto`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
