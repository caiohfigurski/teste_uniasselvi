<?php

# Seta a resposta da consulta da API como JSON.
header('Content-Type: application/json; charset=utf-8');

# Retorna uma string através do POST ...
$_INPUT = file_get_contents("php://input");

# $_POST sempre retorna um array ...
## Se o Post estiver vazio e a variável $inpunt não estiver vazia, faça ...
if (sizeof($_POST) === 0 and empty($_INPUT) === false) {
    # Repassa para constante $_POST o valor convertido de String para Array ...
    $_POST = json_decode($_INPUT, JSON_OBJECT_AS_ARRAY);
}

# Faz o load dinâmico das Classes ...
require 'vendor/autoload.php';

# Inicia o objeto do slimframework e habilita o debug ...
$app = new \Slim\App(['settings' => ['displayErrorDetails' => true]]);

# Importando rotas
require 'rotas/rotaUsuarios.php';
require 'rotas/rotaClientes.php';
require 'rotas/rotaProdutos.php';
require 'rotas/rotaPedidos.php';

# Inicia a API ...
$app->run();
