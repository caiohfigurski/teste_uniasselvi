<?php

// autoload_classmap.php @generated by Composer

$vendorDir = dirname(dirname(__FILE__));
$baseDir = dirname($vendorDir);

return array(
    'Clientes' => $baseDir . '/classes/classClientes.php',
    'Db' => $baseDir . '/classes/classDb.php',
    'Pedidos' => $baseDir . '/classes/classPedidos.php',
    'Produtos' => $baseDir . '/classes/classProdutos.php',
    'Usuarios' => $baseDir . '/classes/classUsuarios.php',
);
