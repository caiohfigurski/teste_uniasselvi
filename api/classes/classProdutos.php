<?php

class Produtos extends Db
{

    public function consultarProduto($id)
    {
        try {
            # Variável que contém o SQL de consulta ...
            $sql = 'SELECT *
                    FROM produtos
                    WHERE IdProduto = :id';

            # Array de Pârametro da consulta ...
            $bind[':id'] = $id;

            # Objeto retornado da consulta ...
            $resultado = $this->consultaDb($sql, $bind);

            # Retorna apenas um objeto ...
            return $resultado->fetch();
        } catch (PDOException $error) {
            print $error->getMessage();
        }
    }

    # Função para listar todas as pessoa do banco ...
    public function listarTodosProdutos()
    {
        try {
    
            $sql = 'SELECT * FROM produtos';

            # Objeto retornado da consulta ...
            $resultado = $this->consultaDb($sql);
           
            return $resultado->fetchAll();
        } catch (PDOException $error) {
            print $error->getMessage();
        }
    }

    # Função para listar todas as pessoa do banco ...
    public function listarProdutos($post = array())
    {
        try {

            $bind = array();

            $post['length'] = isset($post['length']) ? $post['length'] : 20;
            $post['page'] = isset($post['page']) ? (intval($post['page']) - 1) : 0;
            $post['page'] = $post['page'] < 0 ? 0 : $post['page'];

            $start = $post['length'] * $post['page'];

            $sql = 'SELECT * FROM produtos ';

            if (isset($_POST['campoBusca']) and isset($_POST['busca'])) {
                $bind[':busca'] = '%' . $_POST['busca'] . '%';
                $sql .= ' WHERE ' . $_POST['campoBusca'] . ' LIKE :busca ';
            }

            if (isset($_POST['campo']) and isset($_POST['sort'])) {
                $sql .= ' ORDER BY ' . $_POST['campo'] . ' ' . $_POST['sort'];
            }

            $sql .= ' LIMIT ' . $start . ' ,' . $post['length'];

            $resultado = $this->consultaDb($sql, $bind);

            $data = $resultado->fetchAll();

            return [
                'data' => $data,
                'pages' => ceil($this->getMaxOfDb() / $post['length'])
            ];
        } catch (PDOException $error) {
            print $error->getMessage();
        }
    }

    private function getMaxOfDb($filter = array())
    {

        $bind = array();

        $sql = 'SELECT count(1) AS qtd FROM produtos ';

        if (isset($_POST['campoBusca']) and isset($_POST['busca'])) {
            $bind[':busca'] = '%' . $_POST['busca'] . '%';
            $sql .= ' WHERE ' . $_POST['campoBusca'] . ' LIKE :busca ';
        }

        $resultado = $this->consultaDb($sql, $bind);

        $data = $resultado->fetch();

        return  $data['qtd'];
    }

    # Função para inserir uma nova pessoa no banco ...
    public function cadastrarProduto($data)
    {
        try {

            $sql = 'INSERT INTO produtos
                    (
                        NomeProduto
                    ,   DescricaoProduto
                    ,   MarcaProduto
                    ,   PrecoProduto
                    ,   QtdaProduto
                    ,   CodigoBarrasProduto
                    ) 
                    VALUES (
                        :nome
                    ,   :descricao
                    ,   :marca
                    ,   :preco
                    ,   :qtda
                    ,   :codigo_barras
                    )';

            # Array de Pârametro da consulta ...
            $binds = array(
                ':nome' => $data['nome'],
                ':descricao' => $data['descricao'],
                ':marca' => $data['marca'],
                ':preco' => $data['preco'],
                ':qtda' => $data['qtda'],
                ':codigo_barras' => $data['codigo_barras']
            );

            # Objeto executa inserção do objeto no banco ...
            $this->executaDb($sql, $binds);
            # Retorna o id do registro cadastrado  ...
            return $this->lastInsertIdDb();
            /*} else {
                return false;
            } */
        } catch (PDOException $error) {
            print $error->getMessage();
        }
    }

    public function editarProduto($data)
    {
        try {

            $sql = 'UPDATE produtos
                    SET 
                        NomeProduto =           :nome
                    ,   DescricaoProduto =      :descricao
                    ,   MarcaProduto =          :marca
                    ,   PrecoProduto =          :preco
                    ,   QtdaProduto =           :qtda
                    ,   CodigoBarrasProduto =   :codigo_barras
                    WHERE
                        IdProduto = :id
                    ';

            # Array de Pârametro da consulta ...
            $binds = array(
                ':id' => $data['id'],
                ':nome' => $data['nome'],
                ':descricao' => $data['descricao'],
                ':marca' => $data['marca'],
                ':preco' => $data['preco'],
                ':qtda' => $data['qtda'],
                ':codigo_barras' => $data['codigo_barras']
            );

            # Objeto executa inserção do objeto no banco ...
            $this->executaDb($sql, $binds);
        } catch (PDOException $error) {
            print $error->getMessage();
        }
    }

    public function excluirProduto($data)
    {
        try {

            $id = $data;
            $sql1 = 'SELECT * FROM pedidos_de_compras WHERE IdProduto = :id';
            $bind1[':id'] = $id;

            $retorno = $this->executaDb($sql1, $bind1);

            if ($retorno->fetchAll()) {
                echo 'cheio';
                $sql2 = 'DELETE FROM pedidos_de_compras WHERE IdProduto = :id';
                $bind2[':id'] = $id;
                $this->executaDb($sql2, $bind2);
            }

            $sql3 = 'DELETE FROM produtos WHERE IdProduto = :id';
            $bind3[':id'] = $id;

            $this->executaDb($sql3, $bind3);
        } catch (PDOException $error) {
            print $error->getMessage();
        }
    }

    public function excluirVariosProdutos($ids)
    {
        try {


            $arrayIdsProdutos = $ids;
            $arrayClientesComProdutos = [];

            for ($i = 0; $i < sizeof($arrayIdsProdutos); $i++) {

                $sql1 = 'SELECT * FROM pedidos_de_compras WHERE IdProduto = :id';

                # Array de Pârametro da consulta ...
                $bind1[':id'] = $arrayIdsProdutos[$i];

                # Objeto executa inserção do objeto no banco ...
                $retorno = $this->executaDb($sql1, $bind1);

                if ($retorno->fetchAll()) {
                    //echo 'cheio';
                    array_push($arrayClientesComProdutos, $arrayIdsProdutos[$i]);
                } else {
                    $sql2 = 'DELETE FROM produtos WHERE IdProduto = :id';
                    $bind2[':id'] = $arrayIdsProdutos[$i];
                    $this->executaDb($sql2, $bind2);
                }
            }

            for ($i = 0; $i < sizeof($arrayClientesComProdutos); $i++) {

                $sql3 = 'DELETE FROM pedidos_de_compras WHERE IdProduto = :id';

                # Array de Pârametro da consulta ...
                $bind3[':id'] = $arrayClientesComProdutos[$i];

                # Objeto executa inserção do objeto no banco ...
                $this->executaDb($sql3, $bind3);

                $sql4 = 'DELETE FROM produtos WHERE IdProduto = :id';

                # Array de Pârametro da consulta ...
                $bind4[':id'] = $arrayClientesComProdutos[$i];

                # Objeto executa inserção do objeto no banco ...
                $this->executaDb($sql4, $bind4);
            }
        } catch (PDOException $error) {
            print $error->getMessage();
        }
    }
}

?>