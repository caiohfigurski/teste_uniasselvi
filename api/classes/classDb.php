<?php 
# Classe de conexão com Banco de dados.
abstract class Db 
{   
    # Variável estática de conexão com banco de dados ...
    private static $conn = false;

    # Função que estabelece conexão com banco de dados ...
    private static function conectaDB()
    {
        try {
            # Verifica se a conexão já está criada ...
            if (self::$conn === false) {
                    # Estabelece conexão com o Banco de Dados ...
                    self::$conn = new PDO("mysql:host=localhost;dbname=teste_uniasselvi","root","");
                    # Seta globalmente para o banco de dados retornar em array ...
                    self::$conn->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
                    # Seta globalmente modo de exceção a erro ... 
                    self::$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                    # Retorna conexão com o banco de dados ...
                    return self::$conn;
            } else {
                # Retorna conexão ...
                return self::$conn;
            }
        } catch (PDOException $error) {
            print $error->getMessage();
        }
    }

    # Função para realizar consulta no banco de dados ... 
    public static function consultaDb($sql, $bind = null)
    {
        try {
            # Variável de escopo local recebe objeto de conexão ...
            $conn = self::conectaDB();

            # Verifica se há parâmetros para a consulta ...
            if (is_null($bind)) {
                # Retorna consulta ...
                return $conn->query($sql);
            
            # Se existir parâmetros, faça ...
            } else {
                # Prepara a consulta para fazer o bind dos parâmetros ...
                $statement = $conn->prepare($sql);
                # Executa o bind
                $statement->execute($bind);
                # Retorna consulta ...
                return $statement;
            }
        } catch (PDOException $error) {
            print $error->getMessage();
        }
    }

    # Função para realizar inserção de dados no banco de dados ... 
    public static function executaDb($sql, $bind = null)
    {
        try {
            # Variável de escopo local recebe objeto de conexão ...
            $conn = self::conectaDB();
            
            # Prepara a consulta para fazer o bind dos parâmetros ...
            $statement = $conn->prepare($sql);
            # Executa o bind
            $statement->execute($bind);
            # Retorna consulta ...
            return $statement;
        } catch (PDOException $error) {
            print $error->getMessage();
        }
    }

    # Função para retorna quantidade de linhas afetadas ... 
    public static function rowCountDb()
    {
        try {
            # Variável de escopo local recebe objeto de conexão ...
            $conn = self::conectaDB();
            # Retorna as linhas ...
            return $conn->rowCount();
        } catch (PDOException $error) {
            print $error->getMessage();
        }
    }

    # Função para retorna quantidade de linhas afetadas ... 
    public static function lastInsertIdDb()
    {
        try {
            # Variável de escopo local recebe objeto de conexão ...
            $conn = self::conectaDB();
            # Retorna o último Id inserido ...
            return $conn->lastInsertId();
        } catch (PDOException $error) {
            print $error->getMessage();
        }
    }
}
