<?php

class Clientes extends Db
{

    public function consultarCliente($id)
    {
        try {
            # Variável que contém o SQL de consulta ...
            $sql = 'SELECT *
                    FROM clientes
                    WHERE idCliente = :id';

            # Array de Pârametro da consulta ...
            $bind[':id'] = $id;

            # Objeto retornado da consulta ...
            $resultado = $this->consultaDb($sql, $bind);

            # Retorna apenas um objeto ...
            return $resultado->fetch();
        } catch (PDOException $error) {
            print $error->getMessage();
        }
    }

    # Função para listar todas as pessoa do banco ...
    public function listarTodosClientes()
    {
        try {

            $sql = 'SELECT * FROM clientes';

            # Objeto retornado da consulta ...
            $resultado = $this->consultaDb($sql);

            return $resultado->fetchAll();
        } catch (PDOException $error) {
            print $error->getMessage();
        }
    }

    # Função para listar todas as pessoa do banco ...
    public function listarClientes($post = array())
    {
        try {

            $bind = array();

            $post['length'] = isset($post['length']) ? $post['length'] : 20;
            $post['page'] = isset($post['page']) ? (intval($post['page']) - 1) : 0;
            $post['page'] = $post['page'] < 0 ? 0 : $post['page'];

            $start = $post['length'] * $post['page'];

            $sql = 'SELECT * FROM clientes ';

            if (isset($_POST['campoBusca']) and isset($_POST['busca'])) {
                $bind[':busca'] = '%' . $_POST['busca'] . '%';
                $sql .= ' WHERE ' . $_POST['campoBusca'] . ' LIKE :busca ';
            }

            if (isset($_POST['campo']) and isset($_POST['sort'])) {
                $sql .= ' ORDER BY ' . $_POST['campo'] . ' ' . $_POST['sort'];
            }

            $sql .= ' LIMIT ' . $start . ' ,' . $post['length'];

            $resultado = $this->consultaDb($sql, $bind);

            $data = $resultado->fetchAll();

            return [
                'data' => $data,
                'pages' => ceil($this->getMaxOfDb() / $post['length'])
            ];
        } catch (PDOException $error) {
            print $error->getMessage();
        }
    }

    private function getMaxOfDb($filter = array())
    {

        $bind = array();

        $sql = 'SELECT count(1) AS qtd FROM clientes ';

        if (isset($_POST['campoBusca']) and isset($_POST['busca'])) {
            $bind[':busca'] = '%' . $_POST['busca'] . '%';
            $sql .= ' WHERE ' . $_POST['campoBusca'] . ' LIKE :busca ';
        }

        $resultado = $this->consultaDb($sql, $bind);

        $data = $resultado->fetch();

        return  $data['qtd'];
    }

    # Função para inserir uma nova pessoa no banco ...
    public function cadastrarCliente($data)
    {
        try {

            $sql = 'INSERT INTO clientes
                    (
                        NomeCliente
                    ,   CpfCliente
                    ,   DataNascimento
                    ,   EnderecoCliente
                    ,   NumMoradiaCliente
                    ,   BairroCliente
                    ,   CidadeCliente
                    ,   EstadoCliente
                    ,   PaisCliente
                    ,   TelefoneCliente
                    ,   CelularCliente
                    ,   EmailCliente
                    ,   CepCliente
                    ) 
                    VALUES (
                        :nome
                    ,   :cpf
                    ,   :data_nascimento
                    ,   :endereco
                    ,   :numero_moradia
                    ,   :bairro
                    ,   :cidade
                    ,   :estado
                    ,   :pais
                    ,   :telefone
                    ,   :celular
                    ,   :email
                    ,   :cep
                    )';

            # Array de Pârametro da consulta ...
            $binds = array(
                ':nome' => $data['nome'],
                ':cpf' => $data['cpf'],
                ':data_nascimento' => $data['data_nascimento'],
                ':endereco' => $data['endereco'],
                ':numero_moradia' => $data['numero_moradia'],
                ':bairro' => $data['bairro'],
                ':cidade' => $data['cidade'],
                ':estado' => $data['estado'],
                ':pais' => $data['pais'],
                ':telefone' => $data['telefone'],
                ':celular' => $data['celular'],
                ':email' => $data['email'],
                ':cep' => $data['cep']
            );

            # Objeto executa inserção do objeto no banco ...
            $this->executaDb($sql, $binds);
            # Retorna o id do registro cadastrado  ...
            return $this->lastInsertIdDb();
            /*} else {
                return false;
            } */
        } catch (PDOException $error) {
            print $error->getMessage();
        }
    }

    public function editarCliente($data)
    {
        try {

            $sql = 'UPDATE clientes
                    SET 
                        NomeCliente =       :nome
                    ,   CpfCliente =        :cpf
                    ,   DataNascimento =    :data_nascimento
                    ,   EnderecoCliente =   :endereco
                    ,   NumMoradiaCliente = :numero_moradia
                    ,   BairroCliente =     :bairro
                    ,   CidadeCliente =     :cidade
                    ,   EstadoCliente =     :estado
                    ,   PaisCliente =       :pais
                    ,   TelefoneCliente =   :telefone
                    ,   CelularCliente =    :celular
                    ,   EmailCliente =      :email
                    ,   CepCliente =        :cep
                    WHERE
                        idCliente = :id
                    ';

            # Array de Pârametro da consulta ...
            $binds = array(
                ':id' => $data['id'],
                ':nome' => $data['nome'],
                ':cpf' => $data['cpf'],
                ':data_nascimento' => $data['data_nascimento'],
                ':endereco' => $data['endereco'],
                ':numero_moradia' => $data['numero_moradia'],
                ':bairro' => $data['bairro'],
                ':cidade' => $data['cidade'],
                ':estado' => $data['estado'],
                ':pais' => $data['pais'],
                ':telefone' => $data['telefone'],
                ':celular' => $data['celular'],
                ':email' => $data['email'],
                ':cep' => $data['cep']
            );

            # Objeto executa inserção do objeto no banco ...
            $this->executaDb($sql, $binds);
        } catch (PDOException $error) {
            print $error->getMessage();
        }
    }

    public function excluirCliente($data)
    {
        try {

            $id = $data;
            $sql1 = 'SELECT * FROM pedidos_de_compras WHERE IdCliente = :id';
            $bind1[':id'] = $id;

            $retorno = $this->executaDb($sql1, $bind1);

            if ($retorno->fetchAll()) {
                echo 'cheio';
                $sql2 = 'DELETE FROM pedidos_de_compras WHERE IdCliente = :id';
                $bind2[':id'] = $id;
                $this->executaDb($sql2, $bind2);
            }

            $sql3 = 'DELETE FROM clientes WHERE idCliente = :id';
            $bind3[':id'] = $id;

            $this->executaDb($sql3, $bind3);
        } catch (PDOException $error) {
            print $error->getMessage();
        }
    }

    public function excluirVariosClientes($ids)
    {
        try {


            $arrayIdsClientes = $ids;
            $arrayClientesComPedidos = [];

            for ($i = 0; $i < sizeof($arrayIdsClientes); $i++) {

                $sql1 = 'SELECT * FROM pedidos_de_compras WHERE IdCliente = :id';

                # Array de Pârametro da consulta ...
                $bind1[':id'] = $arrayIdsClientes[$i];

                # Objeto executa inserção do objeto no banco ...
                $retorno = $this->executaDb($sql1, $bind1);

                if ($retorno->fetchAll()) {
                    //echo 'cheio';
                    array_push($arrayClientesComPedidos, $arrayIdsClientes[$i]);
                } else {
                    $sql2 = 'DELETE FROM clientes WHERE idCliente = :id';
                    $bind2[':id'] = $arrayIdsClientes[$i];
                    $this->executaDb($sql2, $bind2);
                }
            }

            for ($i = 0; $i < sizeof($arrayClientesComPedidos); $i++) {

                $sql3 = 'DELETE FROM pedidos_de_compras WHERE IdCliente = :id';

                # Array de Pârametro da consulta ...
                $bind3[':id'] = $arrayClientesComPedidos[$i];

                # Objeto executa inserção do objeto no banco ...
                $this->executaDb($sql3, $bind3);

                $sql4 = 'DELETE FROM clientes WHERE IdCliente = :id';

                # Array de Pârametro da consulta ...
                $bind4[':id'] = $arrayClientesComPedidos[$i];

                # Objeto executa inserção do objeto no banco ...
                $this->executaDb($sql4, $bind4);
            }
        } catch (PDOException $error) {
            print $error->getMessage();
        }
    }
}
