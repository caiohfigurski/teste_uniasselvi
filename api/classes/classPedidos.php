<?php

class Pedidos extends Db
{

    public function consultarPedido($id)
    {
        try {
            # Variável que contém o SQL de consulta ...
            $sql = 'SELECT *
                    FROM pedidos_de_compras
                    WHERE IdPedidoCompra = :id';

            # Array de Pârametro da consulta ...
            $bind[':id'] = $id;

            # Objeto retornado da consulta ...
            $resultado = $this->consultaDb($sql, $bind);

            # Retorna apenas um objeto ...
            return $resultado->fetch();
        } catch (PDOException $error) {
            print $error->getMessage();
        }
    }

    # Revisar
    public function listarPedidos($post = array())
    {
        try {

            $bind = array();

            $post['length'] = isset($post['length']) ? $post['length'] : 20;
            $post['page'] = isset($post['page']) ? (intval($post['page']) - 1) : 0;
            $post['page'] = $post['page'] < 0 ? 0 : $post['page'];

            $start = $post['length'] * $post['page'];

            $sql = 'SELECT * FROM pedidos_de_compras ';

            if (isset($_POST['campoBusca']) and isset($_POST['busca'])) {
                $bind[':busca'] = '%' . $_POST['busca'] . '%';
                $sql .= ' WHERE ' . $_POST['campoBusca'] . ' LIKE :busca ';
            }

            if (isset($_POST['campo']) and isset($_POST['sort'])) {
                $sql .= ' ORDER BY ' . $_POST['campo'] . ' ' . $_POST['sort'];
            }

            $sql .= ' LIMIT ' . $start . ' ,' . $post['length'];

            $resultado = $this->consultaDb($sql, $bind);

            $data = $resultado->fetchAll();

            return [
                'data' => $data,
                'pages' => ceil($this->getMaxOfDb() / $post['length'])
            ];
        } catch (PDOException $error) {
            print $error->getMessage();
        }
    }

    private function getMaxOfDb($filter = array())
    {

        $bind = array();

        $sql = 'SELECT count(1) AS qtd FROM pedidos_de_compras ';

        if (isset($_POST['campoBusca']) and isset($_POST['busca'])) {
            $bind[':busca'] = '%' . $_POST['busca'] . '%';
            $sql .= ' WHERE ' . $_POST['campoBusca'] . ' LIKE :busca ';
        }

        $resultado = $this->consultaDb($sql, $bind);

        $data = $resultado->fetch();

        return  $data['qtd'];
    }

    # OK 
    public function cadastrarPedido($data)
    {
        try {

            $sql = 'INSERT INTO pedidos_de_compras
                    (
                        IdUsuario
                    ,   IdCliente
                    ,   IdProduto
                    ,   DataPedidoCompra
                    ,   HorarioPedidoCompra
                    ,   StatusPedidoCompra
                    ,   PrecoUnitarioItensPedido
                    ,   QtdaProdutoItensPedido
                    ,   PrecoTotalItensPedido
                    ) 
                    VALUES (
                        :id_usuario
                    ,   :id_cliente
                    ,   :id_produto
                    ,   :data_pedido_compra
                    ,   :horario_pedido_compra
                    ,   :status_pedido
                    ,   :preco_unitario
                    ,   :qtda_produto
                    ,   :preco_total
                    )';

            # Array de Pârametro da consulta ...
            $binds = array(
                ':id_usuario' => $data['id_usuario'],
                ':id_cliente' => $data['id_cliente'],
                ':id_produto' => $data['id_produto'],
                ':data_pedido_compra' => $data['data_pedido_compra'],
                ':horario_pedido_compra' => $data['horario_pedido_compra'],
                ':status_pedido' => $data['status_pedido'],
                ':preco_unitario' => $data['preco_unitario'],
                ':qtda_produto' => $data['qtda_produto'],
                ':preco_total' => $data['preco_total']
            );

            $this->executaDb($sql, $binds);

            return $this->lastInsertIdDb();
        } catch (PDOException $error) {
            print $error->getMessage();
        }
    }

    public function editarPedido($data)
    {
        try {

            $sql = 'UPDATE pedidos_de_compras
                    SET 
                        IdUsuario =                 :id_usuario
                    ,   IdCliente =                 :id_cliente
                    ,   IdProduto	 =              :id_produto
                    ,   DataPedidoCompra =          :data_pedido
                    ,   HorarioPedidoCompra =       :horario_pedido
                    ,   StatusPedidoCompra =        :status_pedido
                    ,   PrecoUnitarioItensPedido =  :preco_unitario
                    ,   QtdaProdutoItensPedido =    :qtda_produto
                    ,   PrecoTotalItensPedido =     :preco_total
                    WHERE
                        IdPedidoCompra = :id_pedido
                    ';

            # Array de Pârametro da consulta ...
            $binds = array(
                ':id_usuario'     => $data['id_usuario'],
                ':id_cliente'     => $data['id_cliente'],
                ':id_produto'     => $data['id_produto'],
                ':data_pedido'    => $data['data_pedido'],
                ':horario_pedido'    => $data['horario_pedido'],
                ':status_pedido'  => $data['status_pedido'],
                ':preco_unitario' => $data['preco_unitario'],
                ':qtda_produto'   => $data['qtda_produto'],
                ':preco_total'    => $data['preco_total'],
                ':id_pedido'      => $data['id_pedido']
            );

            # Objeto executa inserção do objeto no banco ...
            $this->executaDb($sql, $binds);
        } catch (PDOException $error) {
            print $error->getMessage();
        }
    }

    # OK
    public function excluirPedido($data)
    {
        try {
            var_dump($data);
            $id = $data;
            $sql = 'DELETE FROM pedidos_de_compras WHERE IdPedidoCompra = :id';

            # Array de Pârametro da consulta ...
            $bind[':id'] = $id;

            # Objeto executa inserção do objeto no banco ...
            $this->executaDb($sql, $bind);
        } catch (PDOException $error) {
            print $error->getMessage();
        }
    }

    # OK
    public function excluirVariosPedidos($ids)
    {
        try {

            $arrayIdsPedidos = $ids;

            for ($i = 0; $i < sizeof($arrayIdsPedidos); $i++) {

                $sql = 'DELETE FROM pedidos_de_compras WHERE IdPedidoCompra = :id';

                # Array de Pârametro da consulta ...
                $bind[':id'] = $arrayIdsPedidos[$i];

                # Objeto executa inserção do objeto no banco ...
                $this->executaDb($sql, $bind);
            }
        } catch (PDOException $error) {
            print $error->getMessage();
        }
    }
}
