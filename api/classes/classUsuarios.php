<?php

class Usuarios extends Db {

    # Função para listar uma pessoa ...
    public function loginUsuario($data) {
        try {

            # Variável que contém o SQL de consulta ...
            $sql = 'SELECT *
                    FROM usuarios
                    WHERE 
                        nomeUsuario = :usuario
                    AND
                        senhaUsuario = :senha';

            # Array de Pârametro da consulta ...
            $binds = array(
                ':usuario' => $data['usuario'],
                ':senha' => $data['senha']
            );

            # Objeto retornado da consulta ...
            $resultado = $this->consultaDb($sql, $binds);
            
            # Retorna apenas um objeto ...
            return $resultado->fetch(PDO::FETCH_ASSOC); 
            
        } catch (PDOException $error) {
            print $error->getMessage();
        }
    }
}
?>