<?php 

# Importações ...
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

# OK 
$app->post('/listar_clientes[/{length}[/{page}]]', function (Request $request, Response $response, array $args) {
    $CLIENTES = new Clientes();
    $resposta = $CLIENTES->listarClientes($args);
    $response->getBody()->write(json_encode($resposta, JSON_INVALID_UTF8_IGNORE ??  JSON_UNESCAPED_SLASHES));
    return $response;
});

# OK 
$app->get('/consultar_cliente/{id}', function (Request $request, Response $response, array $args) {
    $id = $args['id'];
    $CLIENTES = new Clientes();
    $resposta = $CLIENTES->consultarCliente($id);
    $response->getBody()->write(json_encode($resposta, JSON_INVALID_UTF8_IGNORE ??  JSON_UNESCAPED_SLASHES));
    return $response;
});

# OK 
$app->get('/listar_todos_clientes', function (Request $request, Response $response) {
    $CLIENTES = new Clientes();
    $resposta = $CLIENTES->listarTodosClientes();
    $response->getBody()->write(json_encode($resposta, JSON_INVALID_UTF8_IGNORE ??  JSON_UNESCAPED_SLASHES));
    return $response;
});

# OK
$app->post('/cadastrar_cliente', function (Request $request, Response $response) {

    $CLIENTES = new Clientes();
    $resposta = $CLIENTES->cadastrarCliente($_POST);

    #  JSON_INVALID_UTF8_IGNORE ?? JSON_UNESCAPED_SLASHES -> Constante nativa do PHP para 'escape' de caracter. 
    # Exemplo: D'Agua ficaria D\'Agua, isso se chama Escape Slash.
    $response->getBody()->write(json_encode($resposta,  JSON_INVALID_UTF8_IGNORE ?? JSON_UNESCAPED_SLASHES));
    return $response;
});

# OK
$app->put('/editar_cliente', function (Request $request, Response $response) {

    $CLIENTES = new Clientes();
    $CLIENTES->editarCliente($_POST);
});

# OK
$app->delete('/excluir_cliente/{id}', function (Request $request, Response $response, array $args) {
    $id = $args['id'];
    $CLIENTES = new Clientes();
    $CLIENTES->excluirCliente($id);
});

# OK 
$app->post('/excluir_varios_clientes', function (Request $request, Response $response) {
    $CLIENTES = new Clientes();
    $CLIENTES->excluirVariosClientes($_POST);
});

?>