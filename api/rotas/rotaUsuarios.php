<?php

# Importações ...
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

# OK
$app->post('/login_usuario', function (Request $request, Response $response) {

    $CLIENTES = new Usuarios();
    $response->getBody()->write(json_encode($CLIENTES->loginUsuario($_POST),  JSON_INVALID_UTF8_IGNORE ?? JSON_UNESCAPED_SLASHES));
    return $response;
});

?>