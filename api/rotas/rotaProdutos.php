<?php 

# Importações ...
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;


# OK 
$app->get('/consultar_produto/{id}', function (Request $request, Response $response, array $args) {
    $id = $args['id'];
    $PRODUTOS = new Produtos();
    $resposta = $PRODUTOS->consultarProduto($id);
    $response->getBody()->write(json_encode($resposta, JSON_INVALID_UTF8_IGNORE ??  JSON_UNESCAPED_SLASHES));
    return $response;
});

# OK 
$app->post('/listar_produtos[/{length}[/{page}]]', function (Request $request, Response $response, array $args) {
    $PRODUTOS = new Produtos();
    $resposta = $PRODUTOS->listarProdutos($args);
    $response->getBody()->write(json_encode($resposta, JSON_INVALID_UTF8_IGNORE ??  JSON_UNESCAPED_SLASHES));
    return $response;
});

# OK 
$app->get('/listar_todos_produtos', function (Request $request, Response $response) {
    $PRODUTOS = new Produtos();
    $resposta = $PRODUTOS->listarTodosProdutos();
    $response->getBody()->write(json_encode($resposta, JSON_INVALID_UTF8_IGNORE ??  JSON_UNESCAPED_SLASHES));
    return $response;
});

# OK
$app->post('/cadastrar_produto', function (Request $request, Response $response) {

    $PRODUTOS = new Produtos();
    $resposta = $PRODUTOS->cadastrarProduto($_POST);

    #  JSON_INVALID_UTF8_IGNORE ?? JSON_UNESCAPED_SLASHES -> Constante nativa do PHP para 'escape' de caracter. 
    # Exemplo: D'Agua ficaria D\'Agua, isso se chama Escape Slash.
    $response->getBody()->write(json_encode($resposta,  JSON_INVALID_UTF8_IGNORE ?? JSON_UNESCAPED_SLASHES));
    return $response;
});

# OK
$app->put('/editar_produto', function (Request $request, Response $response) {

    $PRODUTOS = new Produtos();
    $PRODUTOS->editarProduto($_POST);
});

# OK
$app->delete('/excluir_produto/{id}', function (Request $request, Response $response, array $args) {
    $id = $args['id'];
    $PRODUTOS = new Produtos();
    $PRODUTOS->excluirProduto($id);
});

# OK 
$app->post('/excluir_varios_produtos', function (Request $request, Response $response) {
    $PRODUTOS = new Produtos();
    $PRODUTOS->excluirVariosProdutos($_POST);
});
