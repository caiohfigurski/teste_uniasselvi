<?php 

# Importações ...
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

# OK 
$app->post('/listar_pedidos[/{length}[/{page}]]', function (Request $request, Response $response, array $args) {
    $PEDIDOS = new Pedidos();
    $resposta = $PEDIDOS->listarPedidos($args);
    $response->getBody()->write(json_encode($resposta, JSON_INVALID_UTF8_IGNORE ??  JSON_UNESCAPED_SLASHES));
    return $response;
});

# OK 
$app->get('/consultar_pedido/{id}', function (Request $request, Response $response, array $args) {
    $id = $args['id'];
    $PEDIDOS = new Pedidos();
    $resposta = $PEDIDOS->consultarPedido($id);
    $response->getBody()->write(json_encode($resposta, JSON_INVALID_UTF8_IGNORE ??  JSON_UNESCAPED_SLASHES));
    return $response;
});

# OK
$app->post('/cadastrar_pedido', function (Request $request, Response $response) {

    $PEDIDOS = new Pedidos();
    $resposta = $PEDIDOS->cadastrarPedido($_POST);

    #  JSON_INVALID_UTF8_IGNORE ?? JSON_UNESCAPED_SLASHES -> Constante nativa do PHP para 'escape' de caracter. 
    # Exemplo: D'Agua ficaria D\'Agua, isso se chama Escape Slash.
    $response->getBody()->write(json_encode($resposta,  JSON_INVALID_UTF8_IGNORE ?? JSON_UNESCAPED_SLASHES));
    return $response;
});

# OK
$app->put('/editar_pedido', function (Request $request, Response $response) {
    //var_dump($_POST);
    $PEDIDOS = new Pedidos();
    $PEDIDOS->editarPedido($_POST);
});

# OK
$app->delete('/excluir_pedido/{id}', function (Request $request, Response $response, array $args) {
    $id = $args['id'];
    $PEDIDOS = new Pedidos();
    $PEDIDOS->excluirPedido($id);
});

# OK 
$app->post('/excluir_varios_pedidos', function (Request $request, Response $response) {
    $PEDIDOS = new Pedidos();
    $PEDIDOS->excluirVariosPedidos($_POST);
});
